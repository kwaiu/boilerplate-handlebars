'use strict';

const handlebars = require('gulp-compile-handlebars');
const htmlbeautify = require('gulp-html-beautify');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')({pattern: '*'});
const sassPaths = [
	'bower_components/foundation-sites/scss',
	'bower_components/featherlight/release/',
	'bower_components/owl.carousel/src/scss/'
];

let browserSync = require('browser-sync').create();
let exportfiles = require('./exportfiles.json');

$.sass.compiler = require('node-sass');

/**
 *  $ gulp serve
 * */ 
gulp.task('serve', function() {
	
	browserSync.init({
		server: { baseDir: "./dist" }
	});
	
	gulp.watch('./scss/**/*.scss', gulp.series('sass'));
	gulp.watch('./src/**/*.hbs', gulp.series('html'));		
	gulp.watch('./src/js/*.js', gulp.series('project-js'));

	gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
	gulp.watch("./dist/*.html").on('change', browserSync.reload);
	gulp.watch("./dist/js/**/*.js").on('change', browserSync.reload);
});

/**
 *  $ gulp sass
 * */ 
gulp.task('sass', function () {
	return gulp.src('./scss/**/*.scss')
	.pipe($.sourcemaps.init())
	.pipe($.plumber())
	.pipe($.sass({ includePaths: sassPaths, outputStyle: 'compact' }))	
	.pipe($.sass().on('error', $.sass.logError))
	.pipe($.sourcemaps.write())	
	.pipe(gulp.dest('./dist/css'))
	.pipe($.browserSync.stream())
	.pipe($.uglifycss({ "maxLineLen": 80, "uglyComments": true }))
	.pipe($.rename({ suffix: '.min' }))
	.pipe(gulp.dest('./dist/css'));	
});

/**
 *  $ gulp watch
 * */ 
gulp.task('watch', function () {
	gulp.watch('./scss/**/*.scss', gulp.series('sass'));
	gulp.watch('./src/**/*.hbs', gulp.series('html'));
	gulp.watch('./src/js/app.js', gulp.series('project-js'));
});

/**
 *  $ gulp project-js
 * */ 
gulp.task('project-js', function (cb) {
	$.pump([
		gulp.src(['src/js/app.js']),
		$.uglify(),
		$.rename({suffix: '.min'}),
		gulp.dest('src/js/build'), 
		gulp.dest('dist/js')
	],
	cb);
});

// Export js  
gulp.task('export-js', function() {	
	exportfiles = require('./exportfiles.json');	
	return gulp.src(exportfiles.prodScripts)
	.pipe($.concat('export.js'))
	.pipe(gulp.dest('src/js/build'))
	.pipe(gulp.dest('dist/js'));
});

// Uglify JS 
gulp.task('uglify-js', function (cb) {  
	$.pump([
		gulp.src(['src/js/build/export.js', 'src/js/app.js']),
		$.uglify(),
		$.rename({suffix: '.min'}),
		gulp.dest('src/js/build'), 
		gulp.dest('dist/js')
	],
	cb);
});

// Compile HTML
gulp.task('html', () => {
	return gulp.src('./src/pages/*.hbs')
	.pipe(handlebars({}, {
		ignorePartials: true,
		batch: ['./src/partials']
	}))
	.pipe($.rename({
		extname: '.html'
	}))
	.pipe(gulp.dest('./dist'));
});

// HTML Beautify JS
gulp.task('htmlbeautify', function() {
	var options = { 
		"indent_size": 4, 
		"preserve_newlines": false,
		"end_with_newline": false 
	}
	return gulp.src('./dist/*.html')
	.pipe(htmlbeautify(options))
	.pipe(gulp.dest('./dist/'))
});

exports.default = gulp.series('sass','project-js', 'export-js','uglify-js', 'serve');
exports.build = gulp.series('sass','project-js', 'export-js','uglify-js', 'html', 'htmlbeautify');
